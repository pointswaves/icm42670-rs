//! Report the ICM values over usb as serial using the I2C bus
//!
//! This example is for the bluepill, program it and then connect to the usb port.

#![deny(unsafe_code)]
#![no_std]
#![no_main]

//use panic_halt as _;
use cortex_m_semihosting::hprintln;
use panic_semihosting as _;

use cortex_m::asm::delay;
use cortex_m_rt::entry;
use embedded_hal::digital::v2::OutputPin;
use stm32f1xx_hal::{
    delay::Delay,
    i2c::{BlockingI2c, DutyCycle, Mode},
    pac,
    prelude::*,
    timer::Timer,
    usb::{Peripheral, UsbBus},
};
use usb_device::prelude::*;
use usbd_serial::{SerialPort, USB_CLASS_CDC};

use icm20948::{ICM42670_CHIP_ADR, ICMI2C};

use arrayvec::ArrayString; // 0.4.10
use core::fmt::Write;

#[entry]
fn main() -> ! {
    // Get access to the core peripherals from the cortex-m crate
    let cp = cortex_m::Peripherals::take().unwrap();
    // Get access to the device specific peripherals from the peripheral access crate
    let dp = pac::Peripherals::take().unwrap();

    // Take ownership over the raw flash and rcc devices and convert them into the corresponding
    // HAL structs
    let mut flash = dp.FLASH.constrain();
    let mut rcc = dp.RCC.constrain();
    let mut afio = dp.AFIO.constrain(&mut rcc.apb2);

    // Freeze the configuration of all the clocks in the system and store the frozen frequencies in
    // `clocks`
    let clocks = rcc
        .cfgr
        .use_hse(8.mhz())
        .sysclk(48.mhz())
        .pclk1(24.mhz())
        .freeze(&mut flash.acr);

    assert!(clocks.usbclk_valid());

    let mut gpioa = dp.GPIOA.split(&mut rcc.apb2);
    let mut gpiob = dp.GPIOB.split(&mut rcc.apb2);
    let mut gpioc = dp.GPIOC.split(&mut rcc.apb2);
    let mut led = gpioc.pc13.into_push_pull_output(&mut gpioc.crh);
    led.set_high().unwrap();

    let mut usb_dp = gpioa.pa12.into_push_pull_output(&mut gpioa.crh);
    usb_dp.set_low().unwrap();
    delay(clocks.sysclk().0 / 100);

    let usb = Peripheral {
        usb: dp.USB,
        pin_dm: gpioa.pa11,
        pin_dp: usb_dp.into_floating_input(&mut gpioa.crh),
    };
    let usb_bus = UsbBus::new(usb);

    let mut serial = SerialPort::new(&usb_bus);

    let mut usb_dev = UsbDeviceBuilder::new(&usb_bus, UsbVidPid(0x16c0, 0x27dd))
        .manufacturer("Fake company")
        .product("Serial port")
        .serial_number("TEST")
        .device_class(USB_CLASS_CDC)
        .build();

    for _iii in 0..10 {
        usb_dev.poll(&mut [&mut serial]);
    }

    let scl = gpiob.pb6.into_alternate_open_drain(&mut gpiob.crl);
    let sda = gpiob.pb7.into_alternate_open_drain(&mut gpiob.crl);

    let mut i2c = BlockingI2c::i2c1(
        dp.I2C1,
        (scl, sda),
        &mut afio.mapr,
        Mode::Fast {
            frequency: 100_000.hz(),
            duty_cycle: DutyCycle::Ratio16to9,
        },
        clocks,
        &mut rcc.apb1,
        1000,
        10,
        1000,
        1000,
    );

    led.set_low().unwrap(); // LED on

    let mut delay_obj = Delay::new(cp.SYST, clocks);
    let mut icm_obj = ICMI2C::<_, _, ICM42670_CHIP_ADR>::new(&mut i2c).unwrap();
    icm_obj.init(&mut i2c, &mut delay_obj).unwrap();
    icm_obj.set_lp_filter(&mut i2c, &mut delay_obj).unwrap();

    for _iii in 0..10 {
        usb_dev.poll(&mut [&mut serial]);
    }

    let mut arr_message = ArrayString::<[u8; 128]>::new();

    let timer_tim2 = Timer::tim2(dp.TIM2, &clocks, &mut rcc.apb1);
    let mut countdown = timer_tim2.start_count_down(2.hz());
    countdown.reset();
    let mut counter = 0;

    loop {
        if usb_dev.poll(&mut [&mut serial]) {
            let mut buf = [0u8; 64];

            match serial.read(&mut buf[..]) {
                Ok(_count) => {}
                Err(UsbError::WouldBlock) => {} // No data received
                Err(_err) => {}                 // An error occurred
            };
        }

        if countdown.micros_since() > 100 {
            counter += 1;
            //10_000 == 1 sec
            if counter > 100 {
                let bits = icm_obj.get_values_accel_gyro(&mut i2c).unwrap();
                write!(&mut arr_message, "{:?}\r\n", bits).expect("cant write");
                let (xa, ya, za, xg, yg, zg) = icm_obj.scale_raw_accel_gyro(bits);
                write!(
                    &mut arr_message,
                    "results C {:?} {:?} {:?} {:?} {:?} {:?}",
                    xa, ya, za, xg, yg, zg
                )
                .expect("Can't write");

                match serial.write(&arr_message.as_bytes()) {
                    Ok(_count) => {}
                    Err(UsbError::WouldBlock) => {
                        //hprintln!("Would Block").unwrap();
                    }
                    Err(err) => {
                        hprintln!("Error {:?}", err).unwrap();
                    }
                };
                arr_message.clear();
                write!(&mut arr_message, "\r\n").expect("Can't write");
                //delay_obj.delay_ms(10_u16);
                counter = 0;
            }
            countdown.reset();
        }
    }
}
