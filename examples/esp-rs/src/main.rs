//! Blinks an LED
//!
//! This assumes that a LED is connected to the pin assigned to `led`. (GPIO5)

#![no_std]
#![no_main]

use esp32c3_hal::{
    clock::ClockControl, gpio::IO, i2c::I2C, pac::Peripherals, prelude::*, system::SystemExt,
    timer::TimerGroup, Delay, Rtc,
};
use esp_backtrace as _;
use riscv_rt::entry;

use icm42670::{ICM42670_CHIP_ADR_ALT, ICMI2C};

#[entry]
fn main() -> ! {
    let peripherals = Peripherals::take().unwrap();
    let mut system = peripherals.SYSTEM.split();
    let clocks = ClockControl::boot_defaults(system.clock_control).freeze();

    // Disable the watchdog timers. For the ESP32-C3, this includes the Super WDT,
    // the RTC WDT, and the TIMG WDTs.
    let mut rtc = Rtc::new(peripherals.RTC_CNTL);
    let timer_group0 = TimerGroup::new(peripherals.TIMG0, &clocks);
    let mut wdt0 = timer_group0.wdt;
    let timer_group1 = TimerGroup::new(peripherals.TIMG1, &clocks);
    let mut wdt1 = timer_group1.wdt;

    rtc.swd.disable();
    rtc.rwdt.disable();
    wdt0.disable();
    wdt1.disable();

    // Set GPIO5 as an output, and set its state high initially.
    let io = IO::new(peripherals.GPIO, peripherals.IO_MUX);
    let mut led = io.pins.gpio7.into_push_pull_output();

    // Create a new peripheral object with the described wiring
    // and standard I2C clock speed
    let mut i2c = I2C::new(
        peripherals.I2C0,
        io.pins.gpio10,
        io.pins.gpio8,
        400u32.kHz(),
        &mut system.peripheral_clock_control,
        &clocks,
    )
    .unwrap();

    let mut delay_obj = Delay::new(&clocks);
    let mut icm_obj = ICMI2C::<_, _, ICM42670_CHIP_ADR_ALT>::new(&mut i2c).unwrap();
    icm_obj.init(&mut i2c, &mut delay_obj).unwrap();
    //icm_obj.set_lp_filter(&mut i2c, &mut delay_obj).unwrap();
    led.set_high().unwrap();

    // Initialize the Delay peripheral, and use it to toggle the LED state in a
    // loop.

    loop {
        led.toggle().unwrap();
        delay_obj.delay_ms(500u32);
        let temp = icm_obj.get_values_temp(&mut i2c).unwrap();
        let deg = (temp as f32 / 128.0) + 25.0;
        esp_println::println!("values {temp} {deg}");
        let (xa, ya, za, xg, yg, zg) = icm_obj.get_values_accel_gyro(&mut i2c).unwrap();
        esp_println::println!("values {xa} {ya} {za} {xg} {yg} {zg}");
        let (xa, ya, za, xg, yg, zg) = icm_obj.scale_raw_accel_gyro((xa, ya, za, xg, yg, zg));
        esp_println::println!("values {xa} {ya} {za} {xg} {yg} {zg}");
    }
}
