//! Report the ICM values over usb as serial using the SPI bus
//!
//! This example is for the bluepill, program it and then connect to the usb port.

#![deny(unsafe_code)]
#![no_std]
#![no_main]

//use panic_halt as _;
use cortex_m_semihosting::hprintln;
use panic_semihosting as _;

use cortex_m_rt::entry;
use stm32f1xx_hal::{
    delay::Delay,
    pac,
    prelude::*,
    spi::{Mode, Phase, Polarity, Spi},
};
use embedded_hal::digital::v2::OutputPin;
use icm20948::ICMSPI;

#[entry]
fn main() -> ! {
    // Get access to the core peripherals from the cortex-m crate
    let cp = cortex_m::Peripherals::take().unwrap();
    // Get access to the device specific peripherals from the peripheral access crate
    let dp = pac::Peripherals::take().unwrap();

    // Take ownership over the raw flash and rcc devices and convert them into the corresponding
    // HAL structs
    let mut flash = dp.FLASH.constrain();
    let mut rcc = dp.RCC.constrain();
    let mut _afio = dp.AFIO.constrain(&mut rcc.apb2);
    // Freeze the configuration of all the clocks in the system and store the frozen frequencies in
    // `clocks`
    let clocks = rcc.cfgr.freeze(&mut flash.acr);

    let mut gpiob = dp.GPIOB.split(&mut rcc.apb2);
    let mut gpioc = dp.GPIOC.split(&mut rcc.apb2);
    let mut led = gpioc.pc13.into_push_pull_output(&mut gpioc.crh);

    led.set_high().unwrap();
    let mut cs = gpiob.pb5.into_push_pull_output(&mut gpiob.crl);
    cs.set_high().unwrap();

    let pins = (
        gpiob.pb13.into_alternate_push_pull(&mut gpiob.crh),
        gpiob.pb14.into_floating_input(&mut gpiob.crh),
        gpiob.pb15.into_alternate_push_pull(&mut gpiob.crh),
    ); // (sck, miso, mosi)

    let spi_mode = Mode {
        polarity: Polarity::IdleLow,
        phase: Phase::CaptureOnFirstTransition,
    };
    let mut spi = Spi::spi2(dp.SPI2, pins, spi_mode, 2000.khz(), clocks, &mut rcc.apb1);

    led.set_low().unwrap(); // LED on

    let mut delay_obj = Delay::new(cp.SYST, clocks);
    let mut icm_obj = ICMSPI::new(&mut spi, &mut cs).unwrap();
    icm_obj.init(&mut spi, &mut cs, &mut delay_obj).unwrap();

    loop {
        let bits = icm_obj.get_values_accel_gyro(&mut spi, &mut cs).unwrap();
        hprintln!("{:?}", bits).unwrap();
        let (xa, ya, za, xg, yg, zg) = icm_obj.scale_raw_accel_gyro(bits);
        hprintln!(
            "results C {:?} {:?} {:?} {:?} {:?} {:?}",
            xa,
            ya,
            za,
            xg,
            yg,
            zg
        )
        .unwrap();
        delay_obj.delay_ms(500 as u16);
    }
}
