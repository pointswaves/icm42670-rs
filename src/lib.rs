#![no_std]

//! A platform agnostic Rust driver for the TDK ICM20948, based on the
//! [`embedded-hal`](https://github.com/japaric/embedded-hal) traits.
//!
//! ## The Device
//!
//! The device is a MEMS based 9 degree of freedom ICM
//! The IC contains 3 axis accelerometers, gyroscopes and manometers
//! The IC can comunicate over a SPI or I2C bus
//!
//! ## The Library
//!
//! Contains both I2C and SPI implemntations both sharing common code
//!
//! ## Very basic example
//!
//! this might work on a pi or something similar
//!
//! ```no_run
//! extern crate icm20948;
//! extern crate linux_embedded_hal as hal;
//!
//! use hal::spidev::{self, SpidevOptions};
//! use hal::{Pin, Spidev};
//! use hal::sysfs_gpio::Direction;
//! use hal::Delay;
//!
//! const ICM_CS_PIN: u64 = 8;
//!
//! fn main(){
//!
//!     let mut spi = Spidev::open("/dev/spidev0.0").unwrap();
//!     let options = SpidevOptions::new()
//!         .bits_per_word(8)
//!         .max_speed_hz(20_000)
//!         .build();
//!     spi.configure(&options).unwrap();
//!
//!     let mut cs = Pin::new(ICM_CS_PIN);
//!     cs.export().unwrap();
//!     cs.set_direction(Direction::Out).unwrap();
//!
//!     let mut icm = icm20948::ICMSPI::new(
//!         &mut spi, &mut cs)
//!         .expect("Failed to communicate with icm module!");
//!
//!     icm.init(&mut spi, &mut cs, &mut Delay);
//!
//!     let (xa, ya, za, xg, yg, zg) =
//!            icm_obj.scale_raw_accel_gyro(icm_obj.get_values_accel_gyro(&mut spi, &mut cs).unwrap());
//!     //println!("Sensed, {:?}, {:?}, {:?}, {:?}, {:?}, {:?}", xa, ya, za, xg, yg, zg);
//! }
//! ```

const ICM42X_B0_WHOAMI: u8 = 0x75;
/// ICM20948 default device id from WHOAMI
const ICM42670_CHIP_ID: u8 = 0x67;
/// Default ICM20948 device address
pub const ICM42670_CHIP_ADR: u8 = 0x69;
/// Alternative ICM20948 device address
pub const ICM42670_CHIP_ADR_ALT: u8 = 0x68;
const ICM42X_B0_REG_BANK_SEL: u8 = 0x79;
const ICM42X_B0_LP_CONFIG: u8 = 0x05;
const ICM42X_B0_PWR_MGMT_1: u8 = 0x06;
const ICM42X_B0_PWR_MGMT_2: u8 = 0x07;
const ICM42X_B0_TEMP_XOUT_H: u8 = 0x09;
/// First byte of the accel data
const ICM42X_B0_ACCEL_XOUT_H: u8 = 0x0B;

const ICM42X_B0_GYRO_CONFIG0: u8 = 0x20;
const ICM42X_B0_ACCEL_CONFIG0: u8 = 0x21;

const ICM42X_B0_PWR_MGMT0: u8 = 0x2D;
const _ICM42X_B0_GYRO_XOUT_H: u8 = 0x33;
const ICM42X_B2_GYRO_SMPLRT_DIV_1: u8 = 0x00;
const ICM42X_B2_GYRO_CONFIG: u8 = 0x01;
const ICM42X_B2_GYRO_CONFIG_2: u8 = 0x02;
const ICM42X_B2_ACCEL_SMPLRT_DIV_1: u8 = 0x10;
const ICM42X_B2_ACCEL_CONFIG: u8 = 0x14;
const ICM42X_B2_ACCEL_CONFIG_2: u8 = 0x15;
const _ICM42X_B0_TEMP_OUT_H: u8 = 0x39;

const ICM42X_B0_REG_INT_PIN_CFG: u8 = 0x0F;
const ICM42X_B3_I2C_MST_CTRL: u8 = 0x01;
const ICM42X_B3_I2C_MST_DELAY_CTRL: u8 = 0x02;

const GRAVECONST: f32 = 9.81;

use core::fmt::Debug;
use core::marker::PhantomData;
#[cfg(feature = "cortex-m-debuging")]
use cortex_m_semihosting::hprintln;
use embedded_hal::blocking::delay::DelayMs;
use embedded_hal::blocking::i2c::{Read, Write, WriteRead};
use embedded_hal::blocking::spi::Transfer;
use embedded_hal::digital::v2::OutputPin;

use bit_byte_structs::registers::{
    BitByteStructError, BitStruct, ByteStructI16, CrossByteBitStructI16,
};

use bit_byte_structs::bus::{self, Interface, InterfaceError};

/// Enum used to report errors when using the ICM
///
/// The Enum can report errors that are caused by the ICM libaray its self
/// But can also be used to pass on errors caused by the underling registers or buses
#[derive(Debug)]
pub enum ICMError<E> {
    /// Pass on Error caused by BitByteStructure
    ByteBitReg(BitByteStructError<InterfaceError<E>>),
    /// Pass on Error caused when using the Bus Interface
    Interface(InterfaceError<E>),
    /// Pass on Error caused when using the Bus its self
    ///
    /// Its will be less common for the ICM code to interact with the bus
    /// directly as more code is ported to the BitByteStructure but some
    /// operations may always be done directly, especially code were
    /// performance is important.
    Raw(E),
    /// Stop if the chip at the spesified address is not reporting the corrcet
    /// self identification code.
    ///
    /// For I2C this is most likely if the ID change jumper is in the wrong state or there is
    /// anther chip on the bus with this address.
    ///
    /// For SPI the CS pins may have been mixed up.
    BadChip,
    /// Returned if the register bank is set to a invalid value
    ///
    /// There are 4 banks, 0,1,2 and 3
    BankOutOfRange,
}
impl<E> From<BitByteStructError<InterfaceError<E>>> for ICMError<E> {
    fn from(err: BitByteStructError<InterfaceError<E>>) -> Self {
        ICMError::ByteBitReg(err)
    }
}
impl<E> From<InterfaceError<E>> for ICMError<E> {
    fn from(err: InterfaceError<E>) -> Self {
        ICMError::Interface(err)
    }
}
impl<E> From<E> for ICMError<E> {
    fn from(err: E) -> Self {
        ICMError::Raw(err)
    }
}

/// Enum describing possible ranges of the Accelerometers
pub enum AccelRangeOptions {
    /// +/- 2G
    G2,
    /// +/- 4G
    G4,
    /// +/- 8G
    G8,
    /// +/- 16G
    G16,
}

impl AccelRangeOptions {
    fn as_float(&self) -> f32 {
        // Values taken from table 2 of the data sheet
        match self {
            &AccelRangeOptions::G2 => 16_384.0,
            &AccelRangeOptions::G4 => 8_192.0,
            &AccelRangeOptions::G8 => 4_096.0,
            &AccelRangeOptions::G16 => 2_048.0,
        }
    }
}

/// Enum describing possible ranges of the Gyros
pub enum GyroRangeOptions {
    /// +/- 250 deg/sec
    Deg250,
    /// +/- 500 deg/sec
    Deg500,
    /// +/- 1000 deg/sec
    Deg1000,
    /// +/- 2000 deg/sec
    Deg2000,
}

impl GyroRangeOptions {
    fn as_float(&self) -> f32 {
        match self {
            &GyroRangeOptions::Deg250 => 131.0,
            &GyroRangeOptions::Deg500 => 65.5,
            &GyroRangeOptions::Deg1000 => 32.8,
            &GyroRangeOptions::Deg2000 => 16.4,
        }
    }
}

/// This struct holds and implements the functions that are bus independent for the ICM20948
///
/// The common code is generally the slow and steady bits, like setting up the configureation registers
/// The fast as posible retreave the data may well end up being bus spesific
pub struct ICMCommon<InterfaceThing: ?Sized, E> {
    phantom: PhantomData<InterfaceThing>,
    chip_id: BitStruct<dyn Interface<Error = InterfaceError<E>>, ICM42X_B0_WHOAMI, 8, 0>,
    bank_select: BitStruct<dyn Interface<Error = InterfaceError<E>>, ICM42X_B0_REG_BANK_SEL, 2, 4>,
    power_accel: BitStruct<dyn Interface<Error = InterfaceError<E>>, ICM42X_B0_PWR_MGMT0, 2, 0>,
    power_gyro: BitStruct<dyn Interface<Error = InterfaceError<E>>, ICM42X_B0_PWR_MGMT0, 2, 2>,
    //GYRO_CONFIG0
    //https://invensense.tdk.com/wp-content/uploads/2021/07/DS-000451-ICM-42670-P-v1.0.pdf
    //page 56
    config_gyro_rate: BitStruct<dyn Interface<Error = InterfaceError<E>>, ICM42X_B0_GYRO_CONFIG0, 4, 0>,
    config_accel_rate: BitStruct<dyn Interface<Error = InterfaceError<E>>, ICM42X_B0_ACCEL_CONFIG0, 4, 0>,
    result_temp_registers: ByteStructI16<dyn Interface<Error = InterfaceError<E>>, 1>,
    result_registers: ByteStructI16<dyn Interface<Error = InterfaceError<E>>, 6>,
}

impl<I, E> ICMCommon<I, E>
where
    I: Interface,
    I: ?Sized,
{
    /// Try to create new instance of the ICMCommon struct
    ///
    /// This just sets up all the memory, it dose not try to communicate with the chip yet
    pub fn new() -> Result<Self, BitByteStructError<InterfaceError<E>>> {
        let chip_id =
            BitStruct::<dyn Interface<Error = InterfaceError<E>>, ICM42X_B0_WHOAMI, 8, 0>::new()?;
        let bank_select = BitStruct::<
            dyn Interface<Error = InterfaceError<E>>,
            ICM42X_B0_REG_BANK_SEL,
            2,
            4,
        >::new()?;
        let power_accel =
            BitStruct::<dyn Interface<Error = InterfaceError<E>>, ICM42X_B0_PWR_MGMT0, 2, 0>::new(
            )?;
        let power_gyro =
            BitStruct::<dyn Interface<Error = InterfaceError<E>>, ICM42X_B0_PWR_MGMT0, 2, 2>::new(
            )?;
        let config_gyro_rate =
            BitStruct::<dyn Interface<Error = InterfaceError<E>>, ICM42X_B0_GYRO_CONFIG0, 4, 0>::new(
            )?;
        let config_accel_rate =
            BitStruct::<dyn Interface<Error = InterfaceError<E>>, ICM42X_B0_ACCEL_CONFIG0, 4, 0>::new(
            )?;
        let result_temp_registers = ByteStructI16::<dyn Interface<Error = InterfaceError<E>>, 1>::new(
                ICM42X_B0_TEMP_XOUT_H,
                true,
            )?;
        let result_registers = ByteStructI16::<dyn Interface<Error = InterfaceError<E>>, 6>::new(
            ICM42X_B0_ACCEL_XOUT_H,
            true,
        )?;


        Ok(ICMCommon::<I, E> {
            phantom: PhantomData,
            chip_id,
            bank_select,
            power_accel,
            power_gyro,
            config_gyro_rate,
            config_accel_rate,
            result_temp_registers,
            result_registers,

        })
    }

    /// This checks if the chip reports the correct chipID
    pub fn check_chip(
        &self,
        interface: &mut dyn Interface<Error = InterfaceError<E>>,
    ) -> Result<(), ICMError<E>> {
        self.set_bank(interface, 0)?;
        let chip_id = self.chip_id.read(interface)?;
        let good_id = ICM42670_CHIP_ID;
        #[cfg(feature = "cortex-m-debuging")]
        hprintln!(
            "Chip ID: {:#x} {:#x}, Ok {:?}",
            chip_id,
            good_id,
            good_id == chip_id
        )
        .unwrap();
        if !(good_id == chip_id) {
            return Err(ICMError::BadChip);
        }
        Ok(())
    }

    /// Set the active register bank
    ///
    /// There are 4 register banks, 0, 1, 2 and 3
    pub fn set_bank(
        &self,
        interface: &mut dyn Interface<Error = InterfaceError<E>>,
        bank: u8,
    ) -> Result<(), ICMError<E>> {
        if bank > 3 {
            return Err(ICMError::BankOutOfRange);
        }
        // PWR_MGMT0
        self.bank_select.write(interface, bank)?;
        Ok(())
    }

    /// Set the rate of sampling of the Gyros
    pub fn set_power(
        &self,
        interface: &mut dyn Interface<Error = InterfaceError<E>>,
    ) -> Result<(), ICMError<E>> {
        self.set_bank(interface, 0)?;
        //self.gyro_lp_fact.write(interface, rate)?;
        self.power_accel.write(interface, 0b11)?;
        self.power_gyro.write(interface, 0b11)?;

        Ok(())
    }

      /// Set the rate of sampling of the Gyros
      pub fn set_rate(
        &self,
        interface: &mut dyn Interface<Error = InterfaceError<E>>,
    ) -> Result<(), ICMError<E>> {
        self.set_bank(interface, 0)?;
        //self.gyro_lp_fact.write(interface, rate)?;
        self.config_accel_rate.write(interface, 0b1000)?;
        self.config_gyro_rate.write(interface, 0b1000)?;

        Ok(())
    }
        

    pub fn init(
        &mut self,
        interface: &mut dyn Interface<Error = InterfaceError<E>>,
        delay: &mut dyn DelayMs<u16>,
    ) -> Result<(), ICMError<E>> {
        #[cfg(feature = "cortex-m-debuging")]
        hprintln!("start the init ").unwrap();
        self.set_bank(interface, 0)?;
        self.check_chip(interface)?;
        self.set_power(interface)?;
        self.set_rate(interface)?;

        delay.delay_ms(200);

        return Ok(());
    }
}

/// Pre-Prototype implementation for SPI
///
/// Currently this is just here to make sure that the compiler can
/// work with the spi bus, I dont have a chip set up in spi config
/// to test this with but I will set one up soon.
pub struct ICMSPI<SPI, CS, E> {
    phantom: PhantomData<SPI>,
    phantom_e: PhantomData<E>,
    phantom_cs: PhantomData<CS>,
    comm: ICMCommon<dyn Interface<Error = InterfaceError<E>>, E>,
}
impl<SPI, CS, E> ICMSPI<SPI, CS, E>
where
    SPI: Transfer<u8, Error = E>,
    CS: OutputPin,
{
    pub fn new(spi_bus: &mut SPI, cs: &mut CS) -> Result<Self, E> {
        let new = Self {
            phantom: PhantomData,
            phantom_e: PhantomData,
            phantom_cs: PhantomData,
            comm: ICMCommon::new().unwrap(),
        };
        Ok(new)
    }
    pub fn init(
        &mut self,
        spi_bus: &mut SPI,
        cs: &mut CS,
        delay: &mut dyn DelayMs<u16>,
    ) -> Result<(), ICMError<E>> {
        let mut interface = bus::SPIPeripheral::<SPI, CS, E>::new(spi_bus, cs);
        self.comm.init(&mut interface, delay)
    }
}

/// Prototype implementation for I2C
///
/// The Accelerometer and Gyro data can be retrieved with this prototype
/// code. I am moving more and more of the code to the ICMCommon code and
/// cleaning it up as I go. Once this is complete I will try it out with
/// SPI too.
pub struct ICMI2C<I2C, E, const ADDRESS: u8> {
    phantom: PhantomData<I2C>,
    phantom_e: PhantomData<E>,
    comm: ICMCommon<dyn Interface<Error = InterfaceError<E>>, E>,
}
impl<I2C, E, const ADDRESS: u8> ICMI2C<I2C, E, ADDRESS>
where
    I2C: Read<Error = E> + Write<Error = E> + WriteRead<Error = E>,
{
    pub fn new(i2c_bus: &mut I2C) -> Result<Self, E> {
        let new = Self {
            phantom: PhantomData,
            phantom_e: PhantomData,
            comm: ICMCommon::new().unwrap(),
        };
        Ok(new)
    }

    pub fn init(
        &mut self,
        i2c_bus: &mut I2C,
        delay: &mut dyn DelayMs<u16>,
    ) -> Result<(), ICMError<E>> {
        let mut interface = bus::I2CPeripheral::<I2C, E, ADDRESS>::new(i2c_bus);
        self.comm.init(&mut interface, delay)?;
        Ok(())
    }

    pub fn get_values_accel_gyro(
        &self,
        i2c_bus: &mut I2C,
    ) -> Result<(i16, i16, i16, i16, i16, i16), ICMError<E>> {
        let mut interface = bus::I2CPeripheral::<I2C, E, ADDRESS>::new(i2c_bus);
        let results = self.comm.result_registers.read(&mut interface)?;

        Ok((
            results[0], results[1], results[2], results[3], results[4], results[5],
        ))
    }

    pub fn scale_raw_accel_gyro(
        &self,
        raw: (i16, i16, i16, i16, i16, i16),
    ) -> (f32, f32, f32, f32, f32, f32) {
        let accel_div = AccelRangeOptions::G16.as_float();  //self.accel_range_cache.as_float();
        let gyro_div = GyroRangeOptions::Deg2000.as_float();//self.gyro_range_cache.as_float();
        let standard_grav = 9.80665;
        (
            f32::from(raw.0) / accel_div * standard_grav,
            f32::from(raw.1) / accel_div * standard_grav,
            f32::from(raw.2) / accel_div * standard_grav,
            f32::from(raw.3) / gyro_div,
            f32::from(raw.4) / gyro_div,
            f32::from(raw.5) / gyro_div,
        )
    }
        

    pub fn get_values_temp(
        &self,
        i2c_bus: &mut I2C,
    ) -> Result<i16, ICMError<E>> {
        let mut interface = bus::I2CPeripheral::<I2C, E, ADDRESS>::new(i2c_bus);
        let results = self.comm.result_temp_registers.read(&mut interface)?;

        Ok(
            results[0]
        )
    }
}

/// Prototype implementation for I2C
///
/// This has a simpler API to make it even easier to use if its the only user of the bus
pub struct ICMI2COwned<I2C, E, const ADDRESS: u8> {
    bus: I2C,
    phantom_e: PhantomData<E>,
    comm: ICMCommon<dyn Interface<Error = InterfaceError<E>>, E>,
}
impl<I2C, E, const ADDRESS: u8> ICMI2COwned<I2C, E, ADDRESS>
where
    I2C: Read<Error = E> + Write<Error = E> + WriteRead<Error = E>,
{
    pub fn new(i2c_bus: I2C) -> Result<Self, E> {
        let new = Self {
            bus: i2c_bus,
            phantom_e: PhantomData,
            comm: ICMCommon::new().unwrap(),
        };
        Ok(new)
    }

    pub fn init(&mut self, delay: &mut dyn DelayMs<u16>) -> Result<(), ICMError<E>> {
        let mut interface = bus::I2CPeripheral::<I2C, E, ADDRESS>::new(&mut self.bus);
        self.comm.init(&mut interface, delay)
    }
}
