#![cfg_attr(not(test), no_std)]
use core::fmt::Debug;
use embedded_hal::blocking::delay::DelayMs;
use embedded_hal::blocking::i2c::{Read, Write, WriteRead};
use icm42670::{ICMError, ICM42670_CHIP_ADR, ICMI2C};

struct MockI2C {}

impl MockI2C {
    fn new() -> Self {
        MockI2C {}
    }
}

#[derive(Debug)]
enum MockI2CError {
    Badd,
}

impl Read for MockI2C {
    type Error = MockI2CError;
    fn read(&mut self, address: u8, buffer: &mut [u8]) -> Result<(), Self::Error> {
        buffer[0] = 0;
        // This make the reset code think the chip has reset
        Ok(())
    }
}
impl Write for MockI2C {
    type Error = MockI2CError;
    fn write(&mut self, addr: u8, bytes: &[u8]) -> Result<(), Self::Error> {
        Ok(())
    }
}
impl WriteRead for MockI2C {
    type Error = MockI2CError;
    fn write_read(
        &mut self,
        address: u8,
        bytes: &[u8],
        buffer: &mut [u8],
    ) -> Result<(), Self::Error> {
        Ok(())
    }
}

struct MockDelay {}

impl<UXX> DelayMs<UXX> for MockDelay {
    fn delay_ms(&mut self, ms: UXX) {}
}

#[test]
fn basic_init() {
    let mut i2c = MockI2C::new();
    let mut mock_delay = MockDelay {};
    let mut icm_obj = ICMI2C::<_, _, ICM42670_CHIP_ADR>::new(&mut i2c).unwrap();
    let init_result = icm_obj.init(&mut i2c, &mut mock_delay);
    assert!(matches!(init_result, Err(ICMError::BadChip)));
}
